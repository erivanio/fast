# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Blog.primeira_foto'
        db.delete_column(u'website_blog', 'primeira_foto')

        # Adding field 'Blog.foto'
        db.add_column(u'website_blog', 'foto',
                      self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Blog.foto_externa'
        db.add_column(u'website_blog', 'foto_externa',
                      self.gf(u'django.db.models.fields.CharField')(default='', max_length=255, blank=True),
                      keep_default=False)

        # Adding field 'Blog.foto_interna'
        db.add_column(u'website_blog', 'foto_interna',
                      self.gf(u'django.db.models.fields.CharField')(default='', max_length=255, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Adding field 'Blog.primeira_foto'
        db.add_column(u'website_blog', 'primeira_foto',
                      self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Deleting field 'Blog.foto'
        db.delete_column(u'website_blog', 'foto')

        # Deleting field 'Blog.foto_externa'
        db.delete_column(u'website_blog', 'foto_externa')

        # Deleting field 'Blog.foto_interna'
        db.delete_column(u'website_blog', 'foto_interna')


    models = {
        u'website.blog': {
            'Meta': {'object_name': 'Blog'},
            'foto': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'foto_externa': (u'django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'foto_interna': (u'django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'publicado_em': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'publicar': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '200'}),
            'texto': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'titulo': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'website.opnioes': {
            'Meta': {'object_name': 'Opnioes'},
            'autor': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'cargo': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'logo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'opniao': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'})
        },
        u'website.servicos': {
            'Meta': {'object_name': 'Servicos'},
            'descricao': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'legenda_primeira_foto': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'primeira_foto': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'publicado_em': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'publicar': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '200'})
        },
        u'website.slides': {
            'Meta': {'object_name': 'Slides'},
            'dt_criacao': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            'foto': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'foto_slide': (u'django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'publicar': ('django.db.models.fields.BooleanField', [], {'default': 'True'})
        },
        u'website.solucoes': {
            'Meta': {'object_name': 'Solucoes'},
            'chapeu': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'descricao': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'icone': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'primeira_foto': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'publicar': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'segunda_foto': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '200', 'blank': 'True'})
        }
    }

    complete_apps = ['website']