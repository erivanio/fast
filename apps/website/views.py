from datetime import datetime
from django.core.paginator import Paginator, EmptyPage, InvalidPage
from django.shortcuts import render_to_response, render, get_object_or_404
from django.template import RequestContext
from apps.website.forms import FormContato
from apps.website.models import Slides, Opnioes, Solucoes, Blog
from django.contrib import messages


def homepage(request):
    slides = Slides.objects.filter(publicar=True)
    opnioes = Opnioes.objects.all()
    solucoes = Solucoes.objects.filter(publicar=True)

    return render(request, 'website/index.html', {'slides': slides, 'opnioes': opnioes, 'solucoes': solucoes})


def contato(request):
    if request.method == 'POST':
        form = FormContato(request.POST)
        if form.is_valid():
            form.enviar()
            messages.success(request, 'Mensagem enviada com sucesso!')
    else:
        form = FormContato()
    return render(request, 'website/contato.html', {'form': form})

def solucoes(request):
    solucoes = Solucoes.objects.filter(publicar=True)

    return render(request, 'website/solucoes.html', {'solucoes': solucoes})

def noticia(request, slug, id):

    noticia = get_object_or_404(Blog, slug = slug, id=id, publicar = True)
    ultimas = Blog.objects.filter(publicar = True).exclude(publicado_em__gte=datetime.today())[:4]

    return render_to_response('website/noticia.html', locals(),
                              context_instance=RequestContext(request))

def noticias(request):

    noticias = Blog.objects.filter(publicar=True).exclude(publicado_em__gte=datetime.today()).order_by('-publicado_em')
    ultimas = Blog.objects.filter(publicar = True).exclude(publicado_em__gte=datetime.today())[:4]
    paginator = Paginator(noticias, 3)
    try:
        page = int(request.GET.get('page', '1'))
    except ValueError:
        page = 1
    try:
        noticias = paginator.page(page)
    except (EmptyPage, InvalidPage):
        noticias = paginator.page(paginator.num_pages)
    return render_to_response('website/noticias.html', locals(),
                              context_instance=RequestContext(request))