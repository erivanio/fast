# -*- coding: utf-8 -*-
from django.contrib import admin
from django.contrib.flatpages.models import FlatPage
from django.contrib.flatpages.admin import FlatPageAdmin as FlatPageAdminOld
from django.contrib.flatpages.admin import FlatpageForm as FlatpageFormOld
from django import forms
from ckeditor.widgets import CKEditorWidget
from image_cropping import ImageCroppingMixin
from apps.website.models import Solucoes, Opnioes, Slides, Blog


class FlatpageForm(FlatpageFormOld):
    content = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = FlatPage


class FlatPageAdmin(FlatPageAdminOld):
    form = FlatpageForm


class SlidesAdmin(ImageCroppingMixin, admin.ModelAdmin):
    list_filter = ['nome']
    search_fields = ['nome']

class BlogAdmin(ImageCroppingMixin, admin.ModelAdmin):
    list_filter = ['publicado_em', 'publicar']
    search_fields = ['titulo']
    list_display = ('titulo', 'publicado_em', 'publicar')
    fields = ('titulo', 'texto', 'foto', 'foto_interna', 'foto_externa', 'slug','publicar' )

admin.site.unregister(FlatPage)
admin.site.register(FlatPage, FlatPageAdmin)
admin.site.register(Solucoes)
admin.site.register(Opnioes)
admin.site.register(Blog, BlogAdmin)
admin.site.register(Slides, SlidesAdmin)