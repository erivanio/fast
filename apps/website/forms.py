from django import forms
from django.core.mail import send_mail

__author__ = 'Wouker'

class FormContato(forms.Form):
    nome = forms.CharField(max_length=50)
    email = forms.EmailField(required=True)
    assunto = forms.CharField(max_length=80)
    celular = forms.CharField(max_length=9)
    mensagem = forms.Field(widget=forms.Textarea)

    def enviar(self):
        titulo = 'Komit - Mensagem enviada pelo site - %(assunto)s' % self.cleaned_data
        destino = 'seuemail@gmail.com'
        texto = """
        Nome: %(nome)s
        E-mail: %(email)s
        Assunto: %(assunto)s
        Celular: %(celular)s
        Mensagem:
        %(mensagem)s
        """ % self.cleaned_data

        send_mail(
            subject=titulo,
            message=texto,
            from_email=destino,
            recipient_list=[destino],
        )
