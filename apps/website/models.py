# -*- coding: utf-8 -*-
from datetime import datetime
from django.db import models
from ckeditor.fields import RichTextField
from easy_thumbnails.fields import ThumbnailerImageField
from easy_thumbnails.files import get_thumbnailer
from image_cropping import ImageRatioField
from django.db.models import signals
from django.template.defaultfilters import slugify


class Slides(models.Model):
    nome = models.CharField(max_length=100)
    foto = models.ImageField(upload_to='uploads/slides/', blank=True,
                                 help_text="Foto para slide da index. Dimensões 970x310")
    foto_slide = ImageRatioField('foto', '970x310')
    publicar = models.BooleanField(verbose_name='Publicar', default=True)
    dt_criacao = models.DateTimeField(verbose_name='Data de Criacao', default=datetime.now, blank=True)

    class Meta:
        verbose_name_plural = "Slides"

    def get_foto_slide(self):
        return get_thumbnailer(self.foto).get_thumbnail({
            'size': (970, 310),
            'box': self.foto_slide,
            'crop': True,
            'detail': True,
        }).url

    def __unicode__(self):
        return self.nome


class Opnioes(models.Model):
    autor = models.CharField(max_length=100)
    cargo = models.CharField(max_length=100)
    opniao = RichTextField('Opnião', blank=True, null=True)
    logo = models.ImageField(upload_to='uploads/empresa/', blank=True,
                             help_text="Logo da empresa. Dimensões de no máximo 323x200")

    class Meta:
        verbose_name_plural = "Opniões"

    def __unicode__(self):
        return self.autor


class Solucoes(models.Model):
    nome = models.CharField(max_length=100)
    icone = models.CharField(max_length=100)
    chapeu = RichTextField('Chapéu', blank=True, null=True)
    descricao = RichTextField('Descrição', blank=True, null=True)
    # primeira_foto = models.ImageField(upload_to='uploads/solucoes/', blank=True, null=True)
    # segunda_foto = models.ImageField(upload_to='uploads/solucoes/', blank=True, null=True)
    slug = models.SlugField(max_length=200, blank=True, unique=True)
    publicar = models.BooleanField(verbose_name='Publicar', default=True)

    class Meta:
        verbose_name_plural = "Soluções"

    def __unicode__(self):
        return self.nome



def solucoes_pre_save(signal, instance, sender, **kwargs):
    if not instance.slug:
        instance.slug = slugify(instance.nome)


signals.pre_save.connect(solucoes_pre_save, sender=Solucoes)


class Blog(models.Model):
    titulo = models.CharField(max_length=100)
    texto = RichTextField('Conteúdo', blank=True, null=True)
    foto = models.ImageField(upload_to='uploads/blog/', blank=True, null=True)
    foto_externa = ImageRatioField('foto', '250x180')
    foto_interna = ImageRatioField('foto', '250x250')
    slug = models.SlugField(max_length=200, blank=True, unique=True)
    publicado_em = models.DateTimeField(default=datetime.now)
    publicar = models.BooleanField(verbose_name='Publicar', default=True)

    def __unicode__(self):
        return self.titulo

    def get_absolute_url(self):
        try:
            return '/blog/%s-%s.html' % (self.slug, self.id)
        except:
            pass

    def get_foto_externa(self):
        return get_thumbnailer(self.foto).get_thumbnail({
            'size': (250, 180),
            'box': self.foto_externa,
            'crop': True,
            'detail': True,
            }).url

    def get_foto_interna(self):
        return get_thumbnailer(self.foto).get_thumbnail({
            'size': (250, 250),
            'box': self.foto_interna,
            'crop': True,
            'detail': True,
            }).url

def blog_pre_save(signal, instance, sender, **kwargs):
    if not instance.slug:
        instance.slug = slugify(instance.titulo)

signals.pre_save.connect(blog_pre_save, sender=Blog)

class Servicos(models.Model):
    nome = models.CharField(max_length=100)
    descricao = RichTextField('Descrição', blank=True, null=True)
    primeira_foto = models.ImageField(upload_to='uploads/servicos/', blank=True, null=True)
    legenda_primeira_foto = models.CharField(max_length=200, blank=True, null=True)
    slug = models.SlugField(max_length=200, unique=True)
    publicado_em = models.DateTimeField(default=datetime.now)
    publicar = models.BooleanField(verbose_name='Publicar', default=True)


    def __unicode__(self):
        return self.titulo

    def get_absolute_url(self):
        try:
            return '/blog/%s-%s.html' % (self.slug, self.id)
        except:
            pass
