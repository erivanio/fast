from django.contrib import admin
from apps.financeiro.models import PacoteMensagem, CreditoMensagem, CompraCreditoHistorico, Vendedor

admin.site.register(CreditoMensagem)
admin.site.register(PacoteMensagem)
admin.site.register(CompraCreditoHistorico)
admin.site.register(Vendedor)