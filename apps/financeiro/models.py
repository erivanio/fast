# coding=utf-8
from datetime import datetime
from ckeditor.fields import RichTextField
from django.db import models
from django.db.models.signals import post_save
from apps.mensagem.models import Usuario


class Vendedor(models.Model):
    nome = models.CharField(max_length=200, null=True, blank=True)
    email = models.EmailField(verbose_name='Email', max_length=255, unique=True, db_index=True, )
    telefone = models.CharField(max_length=11)
    endereco = models.CharField('Endereço', max_length=200, null=True, blank=True)
    dt_nasc = models.DateField('Data de Nascimento', null=True, blank=True)

    def __unicode__(self):
        return '%s - %s - %s' % (self.nome, self.email, self.telefone)


class PacoteMensagem(models.Model):
    ativo = models.BooleanField(default=True)
    nome = models.CharField(max_length=150)
    descricao = RichTextField()
    quantidade_inicial = models.IntegerField()
    quantidade_final = models.IntegerField()
    imagem = models.ImageField(upload_to='uploads/planos')
    valor = models.DecimalField(max_digits=3, decimal_places=2)
    usuario = models.ForeignKey(Usuario)
    dt_criacao = models.DateTimeField('Criado em', default=datetime.now)

    def __unicode__(self):
        return '%s - %s' % (self.nome, self.usuario.nome)


class CreditoMensagem(models.Model):
    """Classe de compra de Mensagem"""
    quantidade = models.IntegerField()
    status = models.BooleanField(default=False)
    referencia = models.CharField(max_length=150, blank=True, null=True)
    valor = models.DecimalField(max_digits=10, decimal_places=2)
    usuario = models.ForeignKey(Usuario)
    vendedor = models.ForeignKey(Vendedor, blank=True, null=True)
    pacote_mensagem = models.ForeignKey(PacoteMensagem)
    dt_criacao = models.DateTimeField('Criado em', default=datetime.now)

    def __unicode__(self):
        return '%s' % self.usuario


def atualiza_vendedor(sender, instance, **kwargs):
    if instance.referencia:
        credito_historico = CompraCreditoHistorico.objects.get(referencia__exact=instance.referencia)
        credito_historico.vendedor = instance.vendedor
        credito_historico.save()


post_save.connect(atualiza_vendedor, sender=CreditoMensagem, )


def atualiza_creditos(sender, instance, **kwargs):
    if instance.status:
        instance.usuario.creditos += int(instance.quantidade)
        instance.usuario.save()


post_save.connect(atualiza_creditos, sender=CreditoMensagem, )


class CompraCreditoHistorico(models.Model):
    TIPO_PAGAMENTO_CHOICES = (
        ('1', 'Cartão de crédito'), ('2', 'Boleto'), ('3', 'Débito online (TEF)'), ('4', 'Saldo PagSeguro'),
        ('5', 'Oi Paggo'), ('7', 'Depósito em conta'),)
    usuario = models.ForeignKey(Usuario)
    vendedor = models.ForeignKey(Vendedor, blank=True, null=True)
    pacote_mensagem = models.ForeignKey(PacoteMensagem)
    quantidade = models.IntegerField()
    valor_pacote = models.DecimalField(max_digits=10, decimal_places=2)
    valor_total_compra = models.DecimalField(max_digits=10, decimal_places=2)
    dt_criacao = models.DateTimeField('Criado em', default=datetime.now)
    dt_final = models.DateTimeField('Atualizado em', default=datetime.now)
    tipo_pagamento = models.CharField(max_length=150, blank=True, null=True, choices=TIPO_PAGAMENTO_CHOICES)
    num_parcelas = models.IntegerField(default=0, blank=True, null=True)
    status = models.BooleanField(default=False, )
    referencia = models.CharField(max_length=150, blank=True, null=True)

    def __unicode__(self):
        return '%s - %s - %d' % (self.usuario.nome, self.pacote_mensagem.nome, self.quantidade)



