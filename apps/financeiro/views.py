# coding=utf-8
from datetime import datetime
from django.contrib import messages
from django.core.paginator import Paginator
from django.core.urlresolvers import reverse
from django.db.models import Count
from django.http import HttpResponseRedirect
from django.shortcuts import redirect, render_to_response
from django.template import RequestContext
from django.views.generic import CreateView
from apps.financeiro.forms import AdicionarCreditoMensagemForm
from apps.financeiro.models import CreditoMensagem, PacoteMensagem, CompraCreditoHistorico
from apps.mensagem.models import Usuario
from pagseguro import PagSeguro
from django.contrib.auth.decorators import login_required


class CreditoMensagemCreateView(CreateView):
    model = CreditoMensagem
    form_class = AdicionarCreditoMensagemForm
    template_name = 'servicos/comprar_credito.html'

    def get_context_data(self, **kwargs):
        context = super(CreditoMensagemCreateView, self).get_context_data(**kwargs)
        context['planos'] = PacoteMensagem.objects.filter(ativo__exact=1)
        context['usuario_mensagem'] = \
            Usuario.objects.filter(pk=self.request.user.pk).annotate(num_mensagem=Count('mensagem'))[:1][0]
        return context

    def form_valid(self, form):
        credito_mensagem = form.save(commit=False)
        credito_mensagem.quantidade = int(form.data['quantidade'])
        credito_mensagem.usuario = self.request.user
        pacote_mensagem = PacoteMensagem.objects.filter(ativo__exact=1)

        for pacotes in pacote_mensagem:
            if pacotes.quantidade_inicial < int(credito_mensagem.quantidade) <= pacotes.quantidade_final:
                credito_mensagem.valor = pacotes.valor * int(credito_mensagem.quantidade)
                credito_mensagem.pacote_mensagem = pacotes

        credito_historico = CompraCreditoHistorico()
        credito_historico.usuario = credito_mensagem.usuario
        credito_historico.pacote_mensagem = credito_mensagem.pacote_mensagem
        credito_historico.valor_pacote = credito_mensagem.pacote_mensagem.valor
        credito_historico.quantidade = credito_mensagem.quantidade
        credito_historico.valor_total_compra = credito_mensagem.valor
        credito_historico.dt_criacao = credito_mensagem.dt_criacao
        credito_mensagem.save()

        messages.success(self.request, (
            'Compra efetuada com sucesso! Aguarde a confirmação de pagamento e a liberação dos seus créditos'))

        pg = PagSeguro(email="gabriel@agencia128bits.com", token="195A6CF289DB410293A11ED674AB1D24")
        pg.reference = credito_mensagem.id
        pg.add_item(id=credito_mensagem.id, description=credito_mensagem.pacote_mensagem.nome,
                    amount=credito_mensagem.pacote_mensagem.valor, quantity=credito_mensagem.quantidade, weight=0)
        pg.redirect_url = "http://v2.fasttorpedo.com/notificacao/"
        response = pg.checkout()

        credito_mensagem.referencia = pg.reference
        credito_historico.referencia = pg.reference
        credito_historico.save()
        credito_mensagem.save()

        return redirect(response.payment_url)


def aprovar_pagamento(request):
    if request.GET.has_key('notification_code'):
        notification_code = request.GET['notification_code']
        pg = PagSeguro(email="gabriel@agencia128bits.com", token="195A6CF289DB410293A11ED674AB1D24")
        notification_data = pg.check_transaction(notification_code)
        arquivo_log = file("log_transacao_pagamento", "a")
        arquivo_log.write(notification_data.xml)
        arquivo_log.close()

        credito_mensagem = CreditoMensagem.objects.get(referencia__exact=notification_data.reference)

        if notification_data.status == '3' and credito_mensagem.status == False:
            credito_mensagem.status = True
            credito_mensagem.save()

            credito_historico = CompraCreditoHistorico.objects.get(referencia__exact=notification_data.reference)
            credito_historico.dt_final = datetime.strptime(notification_data.lastEventDate,
                                                           '%Y-%m-%dT%H:%M:%S.000-03:00')
            credito_historico.tipo_pagamento = notification_data.paymentMethod['type']
            credito_historico.num_parcelas = notification_data.installmentCount
            credito_historico.status = True
            #credito_historico.codigo_pagamento = notification_data['code']
            credito_historico.save()

    return redirect('login')


@login_required
def historico_credito(request):
    template = 'servicos/listagem_historico_credito.html'
    historico_credito = CompraCreditoHistorico.objects.filter(usuario=request.user).order_by('-dt_criacao')
    paginator = Paginator(historico_credito, 5)
    page = int(request.GET.get("page", '1'))
    historico_credito = paginator.page(page)
    if request.is_ajax():
        template = 'componentes/historico_credito_objetos.html'

    return render_to_response(template, locals(), context_instance=RequestContext(request))


def busca_credito(request):
    busca_nome = request.GET.get('q')
    busca_dt_inicial = request.GET.get('dt-inicial')
    busca_dt_final = request.GET.get('dt-final')
    historico_credito = CompraCreditoHistorico.objects.filter(usuario=request.user).order_by('-dt_criacao')
    data_inicio = historico_credito[0].dt_criacao

    if busca_nome:
        historico_credito = historico_credito.filter(pacote_mensagem__nome__icontains=busca_nome)

    if busca_dt_inicial and busca_dt_final:
        historico_credito = historico_credito.filter(dt_criacao__range=[busca_dt_inicial, busca_dt_final])

    if busca_dt_inicial and not busca_dt_final:
        historico_credito = historico_credito.filter(dt_criacao__range=[busca_dt_inicial, datetime.now()])

    if busca_dt_final and not busca_dt_inicial:
        historico_credito = historico_credito.filter(dt_criacao__range=[data_inicio, busca_dt_final])

    if busca_nome == '' and busca_dt_inicial == '' and busca_dt_final == '':
        messages.error(request, 'Preencha pelo menos um campo de busca')
        return HttpResponseRedirect(reverse('historico_credito'))

    return render_to_response('servicos/listagem_historico_credito.html', locals(),
                              context_instance=RequestContext(request))
