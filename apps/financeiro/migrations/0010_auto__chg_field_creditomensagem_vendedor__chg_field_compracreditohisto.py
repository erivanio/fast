# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'CreditoMensagem.vendedor'
        db.alter_column(u'financeiro_creditomensagem', 'vendedor_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['financeiro.Vendedor'], null=True))

        # Changing field 'CompraCreditoHistorico.vendedor'
        db.alter_column(u'financeiro_compracreditohistorico', 'vendedor_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['financeiro.Vendedor'], null=True))

    def backwards(self, orm):

        # Changing field 'CreditoMensagem.vendedor'
        db.alter_column(u'financeiro_creditomensagem', 'vendedor_id', self.gf('django.db.models.fields.related.ForeignKey')(default=None, to=orm['financeiro.Vendedor']))

        # Changing field 'CompraCreditoHistorico.vendedor'
        db.alter_column(u'financeiro_compracreditohistorico', 'vendedor_id', self.gf('django.db.models.fields.related.ForeignKey')(default=None, to=orm['financeiro.Vendedor']))

    models = {
        u'financeiro.compracreditohistorico': {
            'Meta': {'object_name': 'CompraCreditoHistorico'},
            'dt_criacao': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'dt_final': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'num_parcelas': ('django.db.models.fields.IntegerField', [], {'default': '0', 'null': 'True', 'blank': 'True'}),
            'pacote_mensagem': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['financeiro.PacoteMensagem']"}),
            'quantidade': ('django.db.models.fields.IntegerField', [], {}),
            'referencia': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'status': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'tipo_pagamento': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'usuario': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mensagem.Usuario']"}),
            'valor_pacote': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'}),
            'valor_total_compra': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'}),
            'vendedor': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['financeiro.Vendedor']", 'null': 'True', 'blank': 'True'})
        },
        u'financeiro.creditomensagem': {
            'Meta': {'object_name': 'CreditoMensagem'},
            'dt_criacao': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'pacote_mensagem': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['financeiro.PacoteMensagem']"}),
            'quantidade': ('django.db.models.fields.IntegerField', [], {}),
            'referencia': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'status': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'usuario': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mensagem.Usuario']"}),
            'valor': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'}),
            'vendedor': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['financeiro.Vendedor']", 'null': 'True', 'blank': 'True'})
        },
        u'financeiro.pacotemensagem': {
            'Meta': {'object_name': 'PacoteMensagem'},
            'ativo': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'descricao': ('ckeditor.fields.RichTextField', [], {}),
            'dt_criacao': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'quantidade_final': ('django.db.models.fields.IntegerField', [], {}),
            'quantidade_inicial': ('django.db.models.fields.IntegerField', [], {}),
            'usuario': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mensagem.Usuario']"}),
            'valor': ('django.db.models.fields.DecimalField', [], {'max_digits': '3', 'decimal_places': '2'})
        },
        u'financeiro.vendedor': {
            'Meta': {'object_name': 'Vendedor'},
            'dt_nasc': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '255', 'db_index': 'True'}),
            'endereco': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'telefone': ('django.db.models.fields.CharField', [], {'max_length': '11'})
        },
        u'mensagem.sociocolaboradores': {
            'Meta': {'object_name': 'SocioColaboradores'},
            'colaborador': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'colaborador'", 'to': u"orm['mensagem.Usuario']"}),
            'contador_mensagem': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'limite': ('django.db.models.fields.IntegerField', [], {}),
            'socio': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'socio'", 'to': u"orm['mensagem.Usuario']"})
        },
        u'mensagem.usuario': {
            'Meta': {'object_name': 'Usuario'},
            'colaboradores': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['mensagem.Usuario']", 'through': u"orm['mensagem.SocioColaboradores']", 'symmetrical': 'False'}),
            'creditos': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'dt_nasc': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '255', 'db_index': 'True'}),
            'empresa': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'endereco': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'profissao': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'telefone': ('django.db.models.fields.CharField', [], {'max_length': '11'})
        }
    }

    complete_apps = ['financeiro']