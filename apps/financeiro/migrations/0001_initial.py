# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'CreditoMensagem'
        db.create_table(u'financeiro_creditomensagem', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('quantidade', self.gf('django.db.models.fields.IntegerField')()),
            ('valor', self.gf('django.db.models.fields.DecimalField')(max_digits=3, decimal_places=2)),
            ('usuario', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mensagem.Usuario'])),
            ('dt_criacao', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
        ))
        db.send_create_signal(u'financeiro', ['CreditoMensagem'])

        # Adding model 'PacoteMensagem'
        db.create_table(u'financeiro_pacotemensagem', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ativo', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('nome', self.gf('django.db.models.fields.CharField')(max_length=150)),
            ('descricao', self.gf('ckeditor.fields.RichTextField')()),
            ('quantidade_inicial', self.gf('django.db.models.fields.IntegerField')()),
            ('quantidade_final', self.gf('django.db.models.fields.IntegerField')()),
            ('valor', self.gf('django.db.models.fields.DecimalField')(max_digits=3, decimal_places=2)),
            ('usuario', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mensagem.Usuario'])),
            ('dt_criacao', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
        ))
        db.send_create_signal(u'financeiro', ['PacoteMensagem'])


    def backwards(self, orm):
        # Deleting model 'CreditoMensagem'
        db.delete_table(u'financeiro_creditomensagem')

        # Deleting model 'PacoteMensagem'
        db.delete_table(u'financeiro_pacotemensagem')


    models = {
        u'financeiro.creditomensagem': {
            'Meta': {'object_name': 'CreditoMensagem'},
            'dt_criacao': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'quantidade': ('django.db.models.fields.IntegerField', [], {}),
            'usuario': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mensagem.Usuario']"}),
            'valor': ('django.db.models.fields.DecimalField', [], {'max_digits': '3', 'decimal_places': '2'})
        },
        u'financeiro.pacotemensagem': {
            'Meta': {'object_name': 'PacoteMensagem'},
            'ativo': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'descricao': ('ckeditor.fields.RichTextField', [], {}),
            'dt_criacao': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'quantidade_final': ('django.db.models.fields.IntegerField', [], {}),
            'quantidade_inicial': ('django.db.models.fields.IntegerField', [], {}),
            'usuario': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mensagem.Usuario']"}),
            'valor': ('django.db.models.fields.DecimalField', [], {'max_digits': '3', 'decimal_places': '2'})
        },
        u'mensagem.sociocolaboradores': {
            'Meta': {'object_name': 'SocioColaboradores'},
            'colaborador': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'colaborador'", 'to': u"orm['mensagem.Usuario']"}),
            'contador_mensagem': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'limite': ('django.db.models.fields.IntegerField', [], {}),
            'socio': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'socio'", 'to': u"orm['mensagem.Usuario']"})
        },
        u'mensagem.usuario': {
            'Meta': {'object_name': 'Usuario'},
            'colaboradores': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['mensagem.Usuario']", 'through': u"orm['mensagem.SocioColaboradores']", 'symmetrical': 'False'}),
            'creditos': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'dt_nasc': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '255', 'db_index': 'True'}),
            'empresa': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'endereco': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'plano': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'profissao': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'telefone': ('django.db.models.fields.CharField', [], {'max_length': '11'})
        }
    }

    complete_apps = ['financeiro']