from django import forms
from apps.financeiro.models import CreditoMensagem


class AdicionarCreditoMensagemForm(forms.ModelForm):
    class Meta:
        model = CreditoMensagem
        fields = ['quantidade']
