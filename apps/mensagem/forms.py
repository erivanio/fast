# -*- coding: utf-8 -*-
from datetime import datetime, time
from django import forms
from django.core.context_processors import request

from django.forms.models import inlineformset_factory
from django.core.exceptions import ValidationError
from django.forms import CheckboxSelectMultiple
import selectable.forms as selectable
from selectable.forms import AutoComboboxSelectWidget, AutoCompleteSelectField

from apps.mensagem.Lookups import UsuariosLookup
from apps.mensagem.models import Usuario, Mensagem, Contato, Grupo, SocioColaboradores, Telefone, HistoricoMensagem


class UserCreationForm(forms.ModelForm):
    telefone = forms.CharField(widget=forms.TextInput(attrs={'type': 'tel'}))
    password1 = forms.CharField(label='Senha', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Confirme a senha', widget=forms.PasswordInput)

    class Meta:
        model = Usuario
        fields = ('email', 'nome', 'telefone', 'endereco', 'empresa', 'profissao','dt_nasc', 'is_superuser', 'is_active')

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("As senhas não conferem")
        return password2

    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        user.is_active = True
        if commit:
            user.save()
        return user


class MensagemForm(forms.ModelForm):
    class Meta:
        model = Mensagem
        fields = ('msg', 'grupo', 'telefone', 'usuario')

    def save(self, commit=True, lista_contato=None):
        if not lista_contato:
            raise ValidationError('Especifique pelo menos um contato ou um grupo')
        mensagem = super(MensagemForm, self).save()
        if commit:
            for telefone in lista_contato:
                telefone_objeto = Telefone.objects.get(pk=int(telefone))
                mensagem.telefone.add(telefone_objeto)
                if self.data['is_api'] != '':
                    mensagem.is_api = True
                if self.data['dt_envio'] and self.data['is_api'] == '':
                    mensagem.dt_envio = datetime.strptime(self.data['dt_envio'], '%d/%m/%Y %H:%M')


                mensagem.save()
                historico_mensagem = HistoricoMensagem()
                historico_mensagem.usuario_remetente = self.cleaned_data['usuario']
                historico_mensagem.contato_destinatario = telefone_objeto.telefone
                historico_mensagem.msg = mensagem
                historico_mensagem.dt_criacao = mensagem.dt_envio
                historico_mensagem.save()
            return mensagem

    def __init__(self, *args, **kwargs):
        current_user = kwargs.pop('usuario')
        super(MensagemForm, self).__init__(*args, **kwargs)
        self.fields['usuario'].queryset = Usuario.objects.filter(pk=current_user.id)
        self.fields['telefone'].queryset = Contato.objects.filter(usuario__exact=current_user)
        self.fields['grupo'].queryset = Grupo.objects.filter(usuario__exact=current_user)


class AdicionarContatoForm(forms.ModelForm):
    class Meta:
        model = Contato

    def __init__(self, *args, **kwargs):
        current_user = kwargs.pop('usuario')
        super(AdicionarContatoForm, self).__init__(*args, **kwargs)
        self.fields['usuario'].queryset = Usuario.objects.filter(pk=current_user.id)

    def clean(self):
        if not self.data['telefone_set-0-telefone']:
            raise ValidationError('Especifique pelo menos um telefone')
        return self.cleaned_data


class AdicionarGrupoForm(forms.ModelForm):
    class Meta:
        model = Grupo

    def __init__(self, *args, **kwargs):
        current_user = kwargs.pop('usuario')
        super(AdicionarGrupoForm, self).__init__(*args, **kwargs)
        self.fields['usuario'].queryset = Usuario.objects.filter(pk=current_user.id)

    def clean(self):
        if not self.data.has_key('grupo-contatos'):
            raise ValidationError('Selecione pelo menos um contato')
        return self.cleaned_data

class AdicionarCreditoColaboradorForm(forms.ModelForm):
    adicionar = forms.IntegerField()

    class Meta:
        model = SocioColaboradores
        fields = ('socio','colaborador','limite','adicionar')

    def __init__(self, *args, **kwargs):
        current_user = kwargs.pop('usuario')
        super(AdicionarCreditoColaboradorForm, self).__init__(*args, **kwargs)
        self.fields['socio'].queryset = Usuario.objects.filter(id=current_user.id)

class AdicionarSocioColaboradorForm(forms.ModelForm):
    colaborador = AutoCompleteSelectField(lookup_class=UsuariosLookup,label='Colaborador',)
    socio = forms.ModelChoiceField(queryset=Usuario.objects.all(), widget=forms.HiddenInput())

    class Meta:
        model = SocioColaboradores
        fields = ('colaborador', 'socio', 'limite', 'descricao')

    def __init__(self, *args, **kwargs):
        current_user = kwargs.pop('usuario')
        super(AdicionarSocioColaboradorForm, self).__init__(*args, **kwargs)
        self.fields['socio'].queryset = Usuario.objects.filter(id=current_user.id)
        self.fields['colaborador'].queryset = Usuario.objects.exclude(id=current_user.id)

