# -*- coding: utf-8 -*-
from datetime import datetime
import json
from urllib import urlencode
from django.contrib import messages
import requests
from rest_framework import status
from rest_framework.authtoken.models import Token
from django.contrib.auth.hashers import make_password
from django.core.urlresolvers import reverse
from django.db.models import Count, Q
from django.forms import TextInput
from django.forms.models import inlineformset_factory
from django.shortcuts import render_to_response, render, redirect
from django.template import RequestContext
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth import authenticate, logout, login as authlogin
from django.contrib.auth.decorators import login_required
from rest_framework.response import Response
from apps.mensagem.forms import UserCreationForm, MensagemForm, AdicionarContatoForm, AdicionarGrupoForm, AdicionarSocioColaboradorForm, AdicionarCreditoColaboradorForm
from apps.financeiro.models import CompraCreditoHistorico, PacoteMensagem
from apps.mensagem.models import Grupo, Usuario, Contato, SocioColaboradores, Telefone, HistoricoMensagem, Mensagem
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from fast import settings
from httplib2 import Http


def gateway_sms(mensagens):
    for sms in mensagens.historicomensagem_set.all():
        destino_numero = '55%s' % sms.contato_destinatario.replace(',', '').replace(' ', '').replace('(', '').replace(')', '').replace('-', '')
        remetente = '55%s' % sms.usuario_remetente.telefone.replace(',', '').replace(' ', '').replace('(', '').replace(')', '').replace('-', '')
        mensagem = sms.msg.msg
        gateway_link = 'http://smsplus.routesms.com:8080/bulksms/bulksms?username=%s&password=%s&type=0&dlr=1&destination=%s&source=%s&message=%s' % \
                       (settings.GATEWAY_LOGIN, settings.GATEWAY_PASSWORD, destino_numero, remetente, mensagem)
        resposta= requests.get(gateway_link)
        sms.status = '1'
        sms.resposta_html = resposta.status_code
        sms.resposta_api = str(resposta.text.split('|')[-1:][0])
        sms.save()
    return True


def homepage(request):
    return ''


def registrar(request):
    form = UserCreationForm()
    if request.method == 'POST':
        form = UserCreationForm(request.POST)

        if form.is_valid():
            form.save()
            Token.objects.create(user=form.instance)
            usuario_login = authenticate(email=form.cleaned_data['email'], password=form.cleaned_data['password1'])
            authlogin(request, usuario_login)
            return render_to_response('servicos/logado.html', locals(), context_instance=RequestContext(request))
        else:
            return render_to_response('website/registrar-usuario.html', locals(),
                                      context_instance=RequestContext(request))
    return render_to_response('website/registrar-usuario.html', locals(), context_instance=RequestContext(request))

@login_required
def editar_perfil(request):
    usuario = request.user
    form = UserCreationForm(instance=usuario)
    if request.method == 'POST':
        data_form = request.POST.copy()
        if data_form['password1'] == '':
            data_form['password1'] = request.POST.get('senha-atual')
            data_form['password2'] = request.POST.get('senha-atual')
        form = UserCreationForm(data_form, instance=usuario)
        if request.user.check_password(data_form.get('senha-atual')):
            if form.is_valid():
                form.save()
                usuario_login = authenticate(email=form.cleaned_data['email'], password=form.cleaned_data['password1'])
                authlogin(request, usuario_login)
                # historico_mensagem = HistoricoMensagem.objects.filter(usuario_remetente=request.user).order_by('-dt_criacao')[:5]
                return HttpResponseRedirect('/login/')
            else:
                return render_to_response('servicos/editar-perfil.html', locals(),
                                          context_instance=RequestContext(request))
        else:
            messages.error(request, 'Senha atual está incorreta')
    return render_to_response('servicos/editar-perfil.html', locals(), context_instance=RequestContext(request))

@login_required
def mensagem_direta(request, pk):
    request.session['contato_pk'] = pk

    return redirect('enviar')


@login_required
def enviar(request):
    form = MensagemForm(usuario=request.user)

    if request.session.has_key('contato_pk'):
        contato_sessao = int(request.session['contato_pk'])
        del request.session['contato_pk']

    data_atual = datetime.now()
    contatos = Contato.objects.filter(usuario_id=request.user.id).order_by('nome')
    grupos = Grupo.objects.filter(usuario_id=request.user.id).order_by('nome')

    if request.method == 'POST':
        form = MensagemForm(request.POST, usuario=request.user)
        usuario = Usuario.objects.get(id=request.user.id)
        lista_contato = request.POST.getlist('contato')
        lista_grupo = request.POST.getlist('grupo')

        for grupo_id in lista_grupo:
            grupo_instancia = Grupo.objects.get(id=grupo_id, usuario_id=request.user.id)
            for contato in grupo_instancia.telefone_set.all():
                if not str(contato.pk) in lista_contato:
                    lista_contato.append(str(contato.pk))

        contador_geral = len(lista_contato)

        if form.is_valid():
            try:
                usuario.creditos -= contador_geral
                if usuario.creditos < 0:
                    messages.error(request, 'Você não possui créditos suficente')
                else:
                    msg = form.save(lista_contato=lista_contato)
                    messages.success(request, 'Mensagem enviada com sucesso')
                    usuario.save()
                    gateway_sms(msg)
                    return redirect('enviar')
            except Exception, e:
                messages.error(request, e.messages[0])
                return render(request, 'servicos/enviar.html', locals())
        else:
            messages.error(request, 'Preencha os campos corretamente')

    return render_to_response('servicos/enviar.html', locals(), context_instance=RequestContext(request))




@login_required
def adicionar(request):
    form_contato = AdicionarContatoForm(usuario=request.user, prefix='contato')
    telefone_formset = inlineformset_factory(Contato, Telefone, can_delete=False, extra=1)
    grupos = Grupo.objects.filter(usuario_id=request.user.id).order_by('nome')
    telefone_formset.form.base_fields["telefone"].widget = TextInput(attrs={'type': 'tel'})
    telefone_formset.form.base_fields["grupo"].queryset = Grupo.objects.filter(usuario_id=request.user.id).order_by('nome')

    if request.method == 'POST':
        if request.POST.has_key('contato-usuario'):
            form_contato = AdicionarContatoForm(request.POST, usuario=request.user, prefix='contato')
            if form_contato.is_valid(): # se o formulario for valido
                contato=form_contato.save(commit=False) # cria um novo contato a partir dos dados enviados
                formset = telefone_formset(request.POST, instance=contato)
                if formset.is_valid():
                    contato.save()
                    formset.save()

                    #A gambis telefone foi criada para suprir a nova funcionalidade de envio pela api
                    #Problema antigo (Quando ia salvar um telefone ele nao tinha usuario, mas agora precisou ter)
                    for gambis_telefone in formset.new_objects:
                        gambis_telefone.usuario_telefone = request.user
                    formset.save()

                    messages.success(request, ('Contato adicionado com sucesso'))
                return redirect('adicionar_contato')
            messages.error(request, form_contato.errors['__all__'].as_text())
            return redirect('adicionar_contato')
    else:
        form_contato = AdicionarContatoForm(usuario=request.user, prefix='contato')
        formset = telefone_formset()
        return render_to_response('servicos/addcontato.html', locals(), context_instance=RequestContext(request))


@login_required
def adicionar_grupo(request):
    form_grupo = AdicionarGrupoForm(usuario=request.user, prefix='grupo')
    todos_contatos = Contato.objects.filter(usuario_id=request.user.id).order_by('nome')

    if request.method == 'POST':
        if request.POST.has_key('grupo-usuario'):
            form_grupo = AdicionarGrupoForm(request.POST, usuario=request.user, prefix='grupo')
            if form_grupo.is_valid():
                form_grupo.save()
                messages.success(request, 'Grupo adicionado com sucesso')
                lista_contato = request.POST.getlist('grupo-contatos')
                for unico_contato in lista_contato:
                    form_grupo.instance.telefone_set.add(unico_contato)
                return redirect('adicionar_grupo') #redirect para contato

            messages.error(request, form_grupo.errors['__all__'].as_text())
            return render_to_response('servicos/addgrupo.html', locals(), context_instance=RequestContext(request))
    else:
        return render_to_response('servicos/addgrupo.html', locals(), context_instance=RequestContext(request))


@login_required
def mostrar_contato(request):
    template='servicos/listagem_contato.html'
    contatos = Contato.objects.filter(usuario_id=request.user.id).order_by('nome')
    paginator = Paginator(contatos, 5)
    page = int(request.GET.get("page", '1'))
    contatos = paginator.page(page)
    if request.is_ajax():
        template = 'componentes/contatos_objetos.html'

    return render_to_response(template, locals(), context_instance=RequestContext(request))


@login_required
def load_contato(request, pk):
    if request.is_ajax():
        contato = Contato.objects.get(pk = pk, usuario_id = request.user.id)

    return render_to_response('componentes/modal_contato.html', locals(), context_instance=RequestContext(request))


@login_required
def historico_sms(request):
    template='servicos/listagem_historico_sms.html'
    historico_mensagem = HistoricoMensagem.objects.filter(usuario_remetente=request.user).order_by('-id')
    contatos = Contato.objects.filter(usuario=request.user)
    paginator = Paginator(historico_mensagem, 5)
    page = int(request.GET.get("page", '1'))
    historico_mensagem = paginator.page(page)
    if request.is_ajax():
        template = 'componentes/historico_sms_objetos.html'

    return render_to_response(template, locals(), context_instance=RequestContext(request))


def busca_sms(request):
    busca_nome = request.GET.get('q')
    busca_dt_inicial = request.GET.get('dt-inicial')
    busca_dt_final = request.GET.get('dt-final')
    historico_mensagem = HistoricoMensagem.objects.filter(usuario_remetente=request.user).order_by('dt_criacao')
    data_inicio = historico_mensagem[0].dt_criacao

    if busca_nome:
        historico_mensagem = historico_mensagem.filter(contato_destinatario__icontains=busca_nome).order_by('-dt_criacao')

    if busca_dt_inicial and busca_dt_final:
        historico_mensagem = historico_mensagem.filter(dt_criacao__range=[busca_dt_inicial, busca_dt_final]).order_by('-dt_criacao')

    if busca_dt_inicial and not busca_dt_final:
        historico_mensagem = historico_mensagem.filter(dt_criacao__range=[busca_dt_inicial, datetime.now()]).order_by('-dt_criacao')

    if busca_dt_final and not busca_dt_inicial:
        historico_mensagem = historico_mensagem.filter(dt_criacao__range=[data_inicio, busca_dt_final]).order_by('-dt_criacao')

    if busca_nome == '' and busca_dt_inicial == '' and busca_dt_final == '':
        messages.error(request, 'Preencha pelo menos um campo de busca')
        return HttpResponseRedirect(reverse('historico_sms'))

    return render_to_response('servicos/listagem_historico_sms.html', locals(), context_instance=RequestContext(request))

@login_required
def contato_grupo(request, pk):
    template='servicos/listagem_contato_grupo.html'
    telefones = Telefone.objects.filter(grupo=pk, usuario_telefone__id=request.user.id).order_by('contato')
    grupo = Grupo.objects.get(pk=pk, usuario_id=request.user.id)
    paginator = Paginator(telefones, 5)
    page = int(request.GET.get("page", '1'))
    telefones = paginator.page(page)
    if request.is_ajax():
        template = 'componentes/contato_grupo_objetos.html'

    return render_to_response(template, locals(), context_instance=RequestContext(request))

@login_required
def mostrar_grupo(request):
    template='servicos/listagem_grupo.html'
    grupos = Grupo.objects.filter(usuario_id=request.user.id).order_by('nome')
    paginator = Paginator(grupos, 5)
    page = int(request.GET.get("page", '1'))
    grupos = paginator.page(page)
    if request.is_ajax():
        template = 'componentes/grupos_objetos.html'

    return render_to_response(template, locals(), context_instance=RequestContext(request))

def busca_contato_grupo(request, pk):
    try:
        queryb = request.GET['q']
        if queryb:
            qset = (Q(contato__nome__icontains=queryb) | Q(telefone__icontains=queryb))
    except:
        queryb = None

    if queryb:
        grupo = Grupo.objects.get(pk=pk)
        telefones = Telefone.objects.filter(qset, grupo=grupo)
    return render_to_response('servicos/listagem_contato_grupo.html', locals(),
                              context_instance=RequestContext(request))

def busca_grupo(request):
    try:
        queryb = request.GET['q']
        if queryb:
            qset = (Q(nome__icontains=queryb))
    except:
        queryb = None

    if queryb:
        grupos = Grupo.objects.filter(qset, usuario=request.user)

    return render_to_response('servicos/listagem_grupo.html', locals(), context_instance=RequestContext(request))


def busca_contato(request):
    try:
        queryb = request.GET['q']
        if queryb:
            qset = (Q(nome__icontains=queryb) | Q(telefone__telefone__icontains=queryb))
    except:
        queryb = None

    if queryb:
        contatos = Contato.objects.filter(qset, usuario=request.user)

    return render_to_response('servicos/listagem_contato.html', locals(), context_instance=RequestContext(request))


def busca_colaborador(request):
    try:
        queryb = request.GET['q']
        if queryb:
            qset = (Q(colaborador__nome__icontains=queryb))
    except:
        queryb = None

    if queryb:
        colaboradores = SocioColaboradores.objects.filter(qset, socio=request.user)

    return render_to_response('servicos/listagem_colaborador.html', locals(), context_instance=RequestContext(request))


@login_required
def mostrar_colaborador(request):
    template='servicos/listagem_colaborador.html'
    colaboradores = SocioColaboradores.objects.filter(socio=request.user)
    paginator = Paginator(colaboradores, 5)
    page = int(request.GET.get("page", '1'))
    colaboradores = paginator.page(page)
    if request.is_ajax():
        template = 'componentes/colaboradores_objetos.html'

    return render_to_response(template, locals(), context_instance=RequestContext(request))

@login_required
def editar(request, pk):
    contato = Contato.objects.get(pk=pk)
    form_contato = AdicionarContatoForm(usuario=request.user, instance=contato)
    telefone_formset = inlineformset_factory(Contato, Telefone, can_delete=True, extra=0)
    telefone_formset.form.base_fields["grupo"].queryset = Grupo.objects.filter(usuario_id=request.user.id).order_by('nome')

    if request.method == 'POST':
        if form_contato:
            form_contato = AdicionarContatoForm(request.POST, usuario=request.user, instance=contato)
            if form_contato.is_valid(): # se o formulario for valido
                form_contato.save(commit=False) # cria um novo contato a partir dos dados enviados
                formset = telefone_formset(request.POST, instance=contato)
                if formset.is_valid():
                    contato.save()
                    formset.save()
                    messages.success(request, ('Contato editado com sucesso'))
                return HttpResponseRedirect('/contatos/', locals())
        else:
            return render_to_response('servicos/editar_contato.html', locals(),
                                      context_instance=RequestContext(request))
    formset = telefone_formset(instance=contato)
    return render_to_response('servicos/editar_contato.html', locals(), context_instance=RequestContext(request))


@login_required
def editar_grupo(request, pk):
    grupo = Grupo.objects.get(pk=pk)
    todos_contatos = Contato.objects.filter(usuario_id=request.user.id).order_by('nome')
    form_grupo = AdicionarGrupoForm(usuario=request.user, instance=grupo)
    if request.method == 'POST':
        if form_grupo:
            form_grupo = AdicionarGrupoForm(request.POST,usuario=request.user,  instance=grupo)
            lista_contato = request.POST.getlist('grupo-contatos')
            if lista_contato:
                if form_grupo.is_valid():
                    form_grupo.save()
                    grupo.telefone_set = []
                    for unico_contato in lista_contato:
                        telefone = Telefone.objects.get(pk=unico_contato)
                        if not telefone in grupo.telefone_set.all():
                            grupo.telefone_set.add(unico_contato)
                    messages.success(request, 'Grupo editado com sucesso')
                    return redirect('grupos')
            else:
                messages.error(request, ('Grupo deve contar ao menos 1 contato'))
                return render_to_response('servicos/editar_grupo.html', locals(), context_instance=RequestContext(request))
    return render_to_response('servicos/editar_grupo.html', locals(), context_instance=RequestContext(request))


@login_required
def deletar_grupo(request, pk):
    u = Grupo.objects.filter(usuario=request.user).get(pk=pk).delete()
    messages.success(request, ('Grupo deletado com sucesso'))
    return HttpResponseRedirect('/grupos/', locals())


@login_required
def importar(request):
    grupos = Grupo.objects.filter(usuario_id=request.user.id).order_by('nome')
    contatos = Contato.objects.filter(usuario_id=request.user.id).order_by('nome')
    cont = 0

    if request.method == 'POST':
        arquivo = request.FILES.get('arquivo')
        arquivo_linha = arquivo.readlines()
        for line in arquivo_linha:
            line = line.split(';')
            if line[0] is not '':
                contato = Contato()
                contato.usuario = request.user
                contato.nome = line[1]
                contato.save()
                telefone = Telefone()
                telefone.telefone = line[0]
                telefone.descricao = ''
                telefone.contato = contato
                telefone.save()
                if request.POST.get('grupo'):
                    lista_grupo = request.POST.getlist('grupo')
                    for g in lista_grupo:
                        grupo = Grupo.objects.get(pk=g)
                        grupo.telefone_set.add(telefone)
        arquivo.close()
        messages.success(request, ('Contato importado com sucesso'))
    return render_to_response('servicos/importar.html', locals(), context_instance=RequestContext(request))


@login_required
def deletar_contato(request, pk):
    u = Contato.objects.filter(usuario=request.user).get(pk=pk).delete()
    messages.success(request, ('Contato deletado com sucesso'))
    return HttpResponseRedirect('/contatos/', locals())


@login_required
def adicionar_colaborador(request):
    form = AdicionarSocioColaboradorForm(usuario=request.user)
    colaboradores = SocioColaboradores.objects.filter(socio=request.user)
    id_colaboradores = []
    possui_pacote = PacoteMensagem.objects.filter(usuario=request.user)
    for i in colaboradores:
        id_colaboradores.append(i.colaborador_id)
    if len(possui_pacote) > 0:
        if request.method == 'POST':
            try:
                form = AdicionarSocioColaboradorForm(request.POST, usuario=request.user)
                usuario = Usuario.objects.get(id=request.user.id)
                colaborador=Usuario.objects.get(id=request.POST.get('colaborador_1'))
                if not int(request.POST.get('colaborador_1')) in id_colaboradores:
                    if int(request.POST.get('limite')) != 0:
                        if usuario.creditos >= int(request.POST.get('limite')):
                            usuario.creditos -= int(request.POST.get('limite'))
                            colaborador.creditos += int(request.POST.get('limite'))
                            if form.is_valid(): # se o formulario for valido
                                form.save() # cria um novo usuario a partir dos dados enviados
                                usuario.save()
                                colaborador.save()
                                messages.success(request, ('Colaborador adicionado com sucesso'))
                                return redirect('colaboradores')
                            else:
                                messages.error(request, ('Dados inválidos'))
                        else:
                            messages.error(request, ('Você não possui créditos suficientes'))
                    else:
                        messages.error(request, ('Coloque no mínimo 1 crédito para o colaborador'))
                else:
                    messages.error(request, ('Colaborador já cadastrado'))
            except Exception, e:
                return render_to_response('servicos/addcolaborador.html', locals(), context_instance=RequestContext(request))
    else:
        messages.error(request, ('Você ainda não comprou um pacote de mensagens'))
    return render_to_response('servicos/addcolaborador.html', locals(), context_instance=RequestContext(request))


@login_required
def adicionar_creditos_colaborador(request, pk):
    socio_colaborador = SocioColaboradores.objects.get(socio=request.user, colaborador=pk)
    if request.method == 'POST':
        form = AdicionarCreditoColaboradorForm(request.POST, usuario=request.user, instance=socio_colaborador)
        usuario = Usuario.objects.get(id=request.user.id)
        if int(form.data['adicionar']) > 0:
            if usuario.creditos >= int(form.data['adicionar']):
                usuario.creditos -= int(form.data['adicionar'])
                if form.is_valid(): # se o formulario for valido
                    form.save(commit=False) # cria um novo usuario a partir dos dados enviados
                    socio_colaborador.limite += int(form.data['adicionar'])
                    socio_colaborador.save()
                    usuario.save()
                    messages.success(request, ('Crédito adicionado com sucesso'))
                    return redirect('colaboradores')
                else:
                    messages.error(request, ('Dados inválidos'))
            else:
                messages.error(request, ('Você não possui créditos suficientes'))
        else:
            messages.error(request, ('Informe um valor positivo'))

    return redirect('colaboradores')


@login_required
def deletar_colaborador(request, pk):
    u = SocioColaboradores.objects.filter(socio=request.user).get(pk=pk).delete()
    messages.success(request, ('Colaborador deletado com sucesso'))
    return HttpResponseRedirect('/colaboradores/', locals())


def login(request):
    #Redirecionar o usuario logado para a tela inicial apos o login
    if request.POST:
        emailUser = request.POST.get('email')
        senhaUser = request.POST.get('password')
        u = authenticate(email=emailUser, password=senhaUser)
        if u is not None:
            if u.is_active:
                authlogin(request, u)
                token = Token.objects.get_or_create(user=request.user)
                historico_mensagem = HistoricoMensagem.objects.filter(usuario_remetente=request.user).order_by('-dt_criacao')[:5]
                return render_to_response('servicos/logado.html', locals(), context_instance=RequestContext(request))
    #Mostrar o usuario ja logado
    if request.user.is_authenticated():
        token = Token.objects.get_or_create(user=request.user)
        historico_mensagem = HistoricoMensagem.objects.filter(usuario_remetente=request.user).order_by('-dt_criacao')[:5]
        return render_to_response('servicos/logado.html', locals(), context_instance=RequestContext(request))
    return render_to_response('website/index.html', locals(), context_instance=RequestContext(request))


def sair(request):
    logout(request)
    return redirect('home')


def api_enviar_sms(request):
    try:
        token = Token.objects.get(key=request.GET.get('token'))
        msg = request.GET.get('mensagem')
        telefone = request.GET.getlist('telefone')
        usuario = Usuario.objects.get(pk=token.user_id)

        telefone_lista = []
        for tel in telefone:
            qnt_digito_telefone = len(tel)
            if qnt_digito_telefone == 10:
                mask_telefone = '(%s) %s-%s' % (tel[:2], tel[2:6], tel[6:10])
            else:
                 mask_telefone = '(%s) %s-%s' % (tel[:2], tel[2:7], tel[7:11])

            if Telefone.objects.filter(telefone=mask_telefone).exists():
                telefone_lista.append(Telefone.objects.filter(telefone=mask_telefone, usuario_telefone=usuario))
            else:
                contato = Contato.objects.create(usuario=usuario, nome='')
                telefone_lista.append(Telefone.objects.get_or_create(telefone=mask_telefone, usuario_telefone=usuario, contato=contato))

        data_hoje = request.GET.get('data')

        if data_hoje is None:
            data_hoje = datetime.now()

        lista_contato = []
        for telefone_indices in telefone_lista:
            lista_contato.append(telefone_indices[0].id)

        data = {'usuario': str(usuario.id), 'msg':msg, 'dt_envio':data_hoje, 'is_api':True}
        form = MensagemForm(data, usuario=usuario)
        if form.is_valid():
            try:
                usuario.creditos -= len(lista_contato)
                if usuario.creditos < 0:
                    return HttpResponse(json.dumps({'mensagem':'Voce nao possui creditos suficente'}), content_type="application/json")
                else:
                    mensagem=form.save(lista_contato=lista_contato)
                    usuario.save()
                    gateway_sms(mensagem)
                    return HttpResponse(json.dumps({'mensagem':'Mensagem enviada com sucesso'}), content_type="application/json")
            except Exception, e:
                return HttpResponse(json.dumps({'mensagem':e.messages[0]}), content_type="application/json")
    except Exception, e:
        return HttpResponse(json.dumps({'mensagem':e.messages[0]}), content_type="application/json")