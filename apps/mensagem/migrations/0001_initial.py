# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Usuario'
        db.create_table(u'mensagem_usuario', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('password', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('last_login', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
            ('email', self.gf('django.db.models.fields.EmailField')(unique=True, max_length=255, db_index=True)),
            ('telefone', self.gf('django.db.models.fields.CharField')(max_length=11)),
            ('nome', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('endereco', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('profissao', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('empresa', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('dt_nasc', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('is_active', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('is_superuser', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('creditos', self.gf('django.db.models.fields.IntegerField')(default=20)),
        ))
        db.send_create_signal(u'mensagem', ['Usuario'])

        # Adding model 'SocioColaboradores'
        db.create_table(u'mensagem_sociocolaboradores', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('socio', self.gf('django.db.models.fields.related.ForeignKey')(related_name='socio', to=orm['mensagem.Usuario'])),
            ('colaborador', self.gf('django.db.models.fields.related.ForeignKey')(related_name='colaborador', to=orm['mensagem.Usuario'])),
            ('contador_mensagem', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('limite', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal(u'mensagem', ['SocioColaboradores'])

        # Adding model 'Grupo'
        db.create_table(u'mensagem_grupo', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nome', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('usuario', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mensagem.Usuario'])),
        ))
        db.send_create_signal(u'mensagem', ['Grupo'])

        # Adding model 'Contato'
        db.create_table(u'mensagem_contato', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nome', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('usuario', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mensagem.Usuario'])),
            ('endereco', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('profissao', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('empresa', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('dt_nasc', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'mensagem', ['Contato'])

        # Adding model 'Telefone'
        db.create_table(u'mensagem_telefone', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('descricao', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('telefone', self.gf('django.db.models.fields.CharField')(max_length=11)),
            ('contato', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mensagem.Contato'])),
        ))
        db.send_create_signal(u'mensagem', ['Telefone'])

        # Adding M2M table for field grupo on 'Telefone'
        m2m_table_name = db.shorten_name(u'mensagem_telefone_grupo')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('telefone', models.ForeignKey(orm[u'mensagem.telefone'], null=False)),
            ('grupo', models.ForeignKey(orm[u'mensagem.grupo'], null=False))
        ))
        db.create_unique(m2m_table_name, ['telefone_id', 'grupo_id'])

        # Adding model 'Mensagem'
        db.create_table(u'mensagem_mensagem', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('msg', self.gf('django.db.models.fields.CharField')(max_length=160)),
            ('usuario', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mensagem.Usuario'])),
            ('dt_criacao', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
            ('is_api', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'mensagem', ['Mensagem'])

        # Adding M2M table for field telefone on 'Mensagem'
        m2m_table_name = db.shorten_name(u'mensagem_mensagem_telefone')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('mensagem', models.ForeignKey(orm[u'mensagem.mensagem'], null=False)),
            ('telefone', models.ForeignKey(orm[u'mensagem.telefone'], null=False))
        ))
        db.create_unique(m2m_table_name, ['mensagem_id', 'telefone_id'])

        # Adding M2M table for field grupo on 'Mensagem'
        m2m_table_name = db.shorten_name(u'mensagem_mensagem_grupo')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('mensagem', models.ForeignKey(orm[u'mensagem.mensagem'], null=False)),
            ('grupo', models.ForeignKey(orm[u'mensagem.grupo'], null=False))
        ))
        db.create_unique(m2m_table_name, ['mensagem_id', 'grupo_id'])

        # Adding model 'HistoricoMensagem'
        db.create_table(u'mensagem_historicomensagem', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('usuario_remetente', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mensagem.Usuario'])),
            ('contato_destinatario', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('msg', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mensagem.Mensagem'])),
            ('dt_criacao', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
            ('status', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'mensagem', ['HistoricoMensagem'])


    def backwards(self, orm):
        # Deleting model 'Usuario'
        db.delete_table(u'mensagem_usuario')

        # Deleting model 'SocioColaboradores'
        db.delete_table(u'mensagem_sociocolaboradores')

        # Deleting model 'Grupo'
        db.delete_table(u'mensagem_grupo')

        # Deleting model 'Contato'
        db.delete_table(u'mensagem_contato')

        # Deleting model 'Telefone'
        db.delete_table(u'mensagem_telefone')

        # Removing M2M table for field grupo on 'Telefone'
        db.delete_table(db.shorten_name(u'mensagem_telefone_grupo'))

        # Deleting model 'Mensagem'
        db.delete_table(u'mensagem_mensagem')

        # Removing M2M table for field telefone on 'Mensagem'
        db.delete_table(db.shorten_name(u'mensagem_mensagem_telefone'))

        # Removing M2M table for field grupo on 'Mensagem'
        db.delete_table(db.shorten_name(u'mensagem_mensagem_grupo'))

        # Deleting model 'HistoricoMensagem'
        db.delete_table(u'mensagem_historicomensagem')


    models = {
        u'mensagem.contato': {
            'Meta': {'object_name': 'Contato'},
            'dt_nasc': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'empresa': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'endereco': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'profissao': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'usuario': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mensagem.Usuario']"})
        },
        u'mensagem.grupo': {
            'Meta': {'object_name': 'Grupo'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'usuario': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mensagem.Usuario']"})
        },
        u'mensagem.historicomensagem': {
            'Meta': {'object_name': 'HistoricoMensagem'},
            'contato_destinatario': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'dt_criacao': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'msg': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mensagem.Mensagem']"}),
            'status': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'usuario_remetente': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mensagem.Usuario']"})
        },
        u'mensagem.mensagem': {
            'Meta': {'object_name': 'Mensagem'},
            'dt_criacao': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'grupo': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['mensagem.Grupo']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_api': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'msg': ('django.db.models.fields.CharField', [], {'max_length': '160'}),
            'telefone': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['mensagem.Telefone']", 'null': 'True', 'blank': 'True'}),
            'usuario': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mensagem.Usuario']"})
        },
        u'mensagem.sociocolaboradores': {
            'Meta': {'object_name': 'SocioColaboradores'},
            'colaborador': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'colaborador'", 'to': u"orm['mensagem.Usuario']"}),
            'contador_mensagem': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'limite': ('django.db.models.fields.IntegerField', [], {}),
            'socio': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'socio'", 'to': u"orm['mensagem.Usuario']"})
        },
        u'mensagem.telefone': {
            'Meta': {'object_name': 'Telefone'},
            'contato': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mensagem.Contato']"}),
            'descricao': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'grupo': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['mensagem.Grupo']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'telefone': ('django.db.models.fields.CharField', [], {'max_length': '11'})
        },
        u'mensagem.usuario': {
            'Meta': {'object_name': 'Usuario'},
            'colaboradores': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['mensagem.Usuario']", 'through': u"orm['mensagem.SocioColaboradores']", 'symmetrical': 'False'}),
            'creditos': ('django.db.models.fields.IntegerField', [], {'default': '20'}),
            'dt_nasc': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '255', 'db_index': 'True'}),
            'empresa': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'endereco': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'profissao': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'telefone': ('django.db.models.fields.CharField', [], {'max_length': '11'})
        }
    }

    complete_apps = ['mensagem']