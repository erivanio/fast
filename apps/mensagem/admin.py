# -*- coding: utf-8 -*-
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from apps.mensagem.forms import UserCreationForm
from apps.mensagem.models import Mensagem, Grupo, Contato, HistoricoMensagem, Usuario, SocioColaboradores, Telefone

class MensagemAdmin(admin.ModelAdmin):
    fieldsets = [
        ("Mensagem", {'fields': ['usuario', 'msg', 'is_api']}), ("Enviar para", {'fields': ['telefone', 'grupo']}),
    ]
    list_display = ('usuario', 'msg', 'dt_criacao')
    list_filter = ['dt_criacao', 'is_api']
    search_fields = ['usuario', 'dt_criacao']


class GrupoAdmin(admin.ModelAdmin):
    list_display = ('nome', 'usuario')
    search_fields = ['nome', 'usuario']

class TelefoneInline(admin.StackedInline):
        model = Telefone
        extra = 1

class ContatoAdmin(admin.ModelAdmin):
    list_display = ('nome', 'usuario')
    inlines = [TelefoneInline,]
    search_fields = ['nome', 'usuario', 'grupo', 'profissao', 'empresa', 'dt_nasc']


class HistoricoAdmin(admin.ModelAdmin):
    fields = ('usuario_remetente', 'contato_destinatario', 'msg', 'status')
    list_display = ('usuario_remetente', 'contato_destinatario', 'msg', 'dt_criacao', 'status')
    list_filter = ['dt_criacao', 'status']
    search_fields = ['contato_destinatario']


class UsuarioAdmin(admin.ModelAdmin):
    form = UserCreationForm
    add_form = UserCreationForm

    list_display = ('email', 'nome', 'telefone')
    list_filter = ('is_superuser', 'is_active')
    fieldsets = (
        (None, {'fields': ('email', 'password1', 'password2')}),
        ('Informação Pessoal', {'fields': ('nome', 'telefone', 'dt_nasc', 'endereco', 'profissao', 'empresa')}),
        ('Permissões', {'fields': ('is_superuser', 'is_active')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2')}
        ),
    )
    search_fields = ('email', 'nome', 'endereco', 'profissao', 'empresa')
    ordering = ('email',)


class SocioColaboradoresAdmin(admin.ModelAdmin):
    list_display = ('socio', 'colaborador')
    search_fields = ['socio', 'colaborador']


admin.site.register(SocioColaboradores, SocioColaboradoresAdmin)
admin.site.register(Usuario, UsuarioAdmin)
admin.site.register(Mensagem, MensagemAdmin)
admin.site.register(Grupo, GrupoAdmin)
admin.site.register(Contato, ContatoAdmin)
admin.site.register(HistoricoMensagem, HistoricoAdmin)