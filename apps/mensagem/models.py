# -*- coding: utf-8 -*-
from datetime import datetime
from django.core.exceptions import ValidationError
from django.db import models
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser


class UsuarioManager(BaseUserManager):
    def create_user(self, email, nome, password=None):
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(email=self.normalize_email(email), nome=nome)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, nome, password):
        user = self.create_user(email, nome, password=password, )
        user.is_superuser = True
        user.save(using=self._db)
        return user


class Usuario(AbstractBaseUser):
    email = models.EmailField(verbose_name='Email', max_length=255, unique=True, db_index=True, )
    colaboradores = models.ManyToManyField('self', through='SocioColaboradores', symmetrical=False)
    telefone = models.CharField(max_length=15)
    nome = models.CharField(max_length=200, null=True, blank=True)
    endereco = models.CharField('Endereço', max_length=200, null=True, blank=True)
    profissao = models.CharField('Profissão', max_length=200, null=True, blank=True)
    empresa = models.CharField(max_length=200, null=True, blank=True)
    dt_nasc = models.DateField('Data de Nascimento', null=True, blank=True)
    is_active = models.BooleanField('Ativo?', default=True)
    is_superuser = models.BooleanField('Administrador?', default=False)
    creditos = models.IntegerField('Créditos', default=20)

    objects = UsuarioManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['telefone']

    class Meta:
        verbose_name = 'Usuário'
        verbose_name_plural = 'Usuários'

    def get_full_name(self):
        return self.email

    def get_short_name(self):
        return self.email

    def __unicode__(self):
        return self.email

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True

    @property
    def is_staff(self):
        return self.is_superuser


class SocioColaboradores(models.Model):
    socio = models.ForeignKey(Usuario, related_name='socio')
    colaborador = models.ForeignKey(Usuario, related_name='colaborador')
    descricao = models.CharField('Descrição', max_length=200, null=True, blank=True)
    contador_mensagem = models.IntegerField(default=0)
    limite = models.IntegerField()

    class Meta:
        verbose_name = 'Socio e Colaborador'
        verbose_name_plural = 'Sócios e Colaboradores'

    def __unicode__(self):
        return '%s' % self.socio

    def get_credito_disponivel(self):
        disponivel = self.limite - self.contador_mensagem
        return disponivel

    def clean(self):
        try:
            colaborador = self.colaborador
        except:
            raise ValidationError('Usuário não cadastrado')


class Grupo(models.Model):
    nome = models.CharField(max_length=200)
    usuario = models.ForeignKey(Usuario)

    def __unicode__(self):
        return self.nome


class Contato(models.Model):
    nome = models.CharField(max_length=200, null=True, blank=True)
    usuario = models.ForeignKey(Usuario)
    endereco = models.CharField('Endereço', max_length=200, null=True, blank=True)
    profissao = models.CharField('Profissão', max_length=200, null=True, blank=True)
    empresa = models.CharField(max_length=200, null=True, blank=True)
    dt_nasc = models.DateField('Data de Nascimento', null=True, blank=True)

    def __unicode__(self):
        return self.nome


class Telefone(models.Model):
    descricao = models.CharField(max_length=200, null=True, blank=True)
    #variavel com o nome usuario_telefone ao invez de usuario por conta de um problema no south
    usuario_telefone = models.ForeignKey(Usuario, blank=True, null=True, related_name='usuario_telefone')
    telefone = models.CharField(max_length=15)
    contato = models.ForeignKey(Contato)
    grupo = models.ManyToManyField(Grupo, null=True, blank=True)

    def __unicode__(self):
        return self.telefone

    def clean(self):
        telefones = Telefone.objects.filter(telefone=self.telefone, usuario_telefone=self.usuario_telefone)
        if telefones.exists():
            raise ValidationError('O telefone cadastrado ja existe')


class Mensagem(models.Model):
    msg = models.CharField('Mensagem', max_length=160)
    usuario = models.ForeignKey(Usuario)
    dt_criacao = models.DateTimeField('Criado em', default=datetime.now)
    dt_envio = models.DateTimeField('Data de Envio', default=datetime.now)
    is_api = models.BooleanField('Api?', default=False)
    telefone = models.ManyToManyField(Telefone, null=True, blank=True)
    grupo = models.ManyToManyField(Grupo, null=True, blank=True)

    class Meta:
        verbose_name = 'Mensagem'
        verbose_name_plural = 'Mensagens'

    def __unicode__(self):
        return '%s' % self.msg


class HistoricoMensagem(models.Model):
    STATUS_SMS_CHOICES = (('1', 'Pendente'), ('2', 'Enviada'), ('3', 'Cancelada'))

    usuario_remetente = models.ForeignKey(Usuario)
    contato_destinatario = models.CharField(max_length=200)
    msg = models.ForeignKey(Mensagem)
    dt_criacao = models.DateTimeField('Criado em', default=datetime.now)
    status = models.CharField(max_length=2, blank=True, null=True, choices=STATUS_SMS_CHOICES)
    resposta_html = models.CharField(max_length=10, blank=True, null=True)
    resposta_api = models.CharField(max_length=50, blank=True, null=True)

    def __unicode__(self):
        return '%s' % self.dt_criacao


