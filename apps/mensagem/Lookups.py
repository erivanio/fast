from selectable.base import ModelLookup
from selectable.decorators import login_required
from selectable.registry import registry
from apps.mensagem.models import Usuario


@login_required
class UsuariosLookup(ModelLookup):
    model = Usuario
    search_field = 'email__icontains'
    filters = {'is_active': True, }

    def get_query(self, request, term):
        #colaborador = SocioColaboradores.objects.filter(socio=request.user)
        resultado = Usuario.objects.filter(email__startswith=term).exclude(id=request.user.id)
        return resultado

registry.register(UsuariosLookup)