from django.conf.urls import patterns, include, url

from django.contrib import admin
from django.contrib.auth.views import password_reset, password_reset_done, password_reset_confirm, password_reset_complete
from apps.financeiro.views import CreditoMensagemCreateView
import settings

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^media/(.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
    (r'^ckeditor/', include('ckeditor.urls')),
    url(r'^pages/', include('django.contrib.flatpages.urls')),

    url(r'^esqueceu-a-senha/$', password_reset,
        {'post_reset_redirect' : '/esqueceu-a-senha/concluido/'}, name='esqueceu_senha'),
    (r'^esqueceu-a-senha/concluido/$', password_reset_done),
    url(r'^esqueceu-a-senha/reset/(?P<uidb36>[0-9A-Za-z]+)-(?P<token>.+)/$', password_reset_confirm,
        {'post_reset_redirect' : '/esqueceu-a-senha/completo/'}),
    (r'^esqueceu-a-senha/completo/$', password_reset_complete),
    (r'^selectable/', include('selectable.urls')),
)

urlpatterns += patterns('apps.mensagem.views',
    url(r'login/$', 'login', name='login'),
    url(r'^sair/$', 'sair'),
    url(r'^registrar/$', 'registrar'), # pagina de cadastro
    url(r'^editar-perfil/$', 'editar_perfil', name='editar-perfil'), # pagina de cadastro
    url(r'^mensagem/$', 'enviar', name='enviar'), # pagina de envio
    url(r'^mensagem-direta/(\d+)/$', 'mensagem_direta'),
    url(r'^adicionar-contato/$', 'adicionar', name='adicionar_contato'),
    url(r'^adicionar-grupo/$', 'adicionar_grupo', name='adicionar_grupo'),
    url(r'^importar/$', 'importar'),
    url(r'^editar-contato/(\d+)/$', 'editar'),
    url(r'^contatos/$', 'mostrar_contato', name='contatos'),
    url(r'^load-contato/(\d+)/$', 'load_contato',name='load_contato'),
    url(r'^grupos/$', 'mostrar_grupo', name='grupos'),
    url(r'^grupo/(\d+)/contatos/$', 'contato_grupo', name='contato_grupo'),
    url(r'^editar-grupo/(\d+)/$', 'editar_grupo'),
    url(r'^colaboradores/$', 'mostrar_colaborador', name='colaboradores'),
    url(r'^adicionar_colaborador/(\d+)/$', 'deletar_colaborador'),
    url(r'^deletar_contato/(\d+)/$', 'deletar_contato'),
    url(r'^deletar_grupo/(\d+)/$', 'deletar_grupo'),
    url(r'^deletar_colaborador/(\d+)/$', 'deletar_colaborador'),
    url(r'^adicionar_creditos_colaborador/(\d+)/$', 'adicionar_creditos_colaborador'),
    url(r'^registrar/$', 'registrar', name='registrar'),
    url(r'^deletar-contato/(\d+)/$', 'deletar_contato'),
    url(r'^deletar-grupo/(\d+)/$', 'deletar_grupo'),
    url(r'^deletar-colaborador/(\d+)/$', 'deletar_colaborador'),
    url(r'^adicionar-colaborador/$', 'adicionar_colaborador'),
    url(r'^busca-grupo/$', 'busca_grupo'),
    url(r'^busca-colaborador/$', 'busca_colaborador'),
    url(r'^busca-contato/$', 'busca_contato'),
    url(r'^busca-contato-grupo/(\d+)/$', 'busca_contato_grupo'),
    url(r'^busca-sms/$', 'busca_sms'),
    url(r'^historico-sms/$', 'historico_sms', name='historico_sms'),
    url(r'^api/enviar-sms/$', 'api_enviar_sms', name='api_enviar_sms'),
)

urlpatterns += patterns('apps.website.views',
    url(r'^$', 'homepage', name='home'),
    url(r'^contato/$', 'contato', name='contato'),
    url(r'^solucoes/$', 'solucoes', name='solucoes'),
    url(r'^blog/(?P<slug>[\w_-]+)-(?P<id>\d+).html', 'noticia'),
    url(r'^blog/', 'noticias', name='blog')
)

urlpatterns += patterns('apps.financeiro.views',
    url(r'comprar-credito/$', CreditoMensagemCreateView.as_view(), name='comprar_credito'),
    url(r'notificacao/$', 'aprovar_pagamento', name='notificacao'),
    url(r'^historico-credito/$', 'historico_credito', name='historico_credito'),
    url(r'^busca-credito/$', 'busca_credito'),
)