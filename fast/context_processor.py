from django.db.models import Count
from apps.mensagem.models import Usuario


def estaticos(request):
    if request.user.pk:
        usuario_mensagem = Usuario.objects.filter(pk=request.user.pk).annotate(num_mensagem=Count('mensagem__historicomensagem'))[:1][0]
        colaborador_contador = Usuario.objects.filter(pk=request.user.pk).annotate(num_colaborador=Count('colaboradores'))[:1][0]
        contatos_contador = Usuario.objects.filter(pk=request.user.pk).annotate(num_contatos=Count('contato'))[:1][0]
        grupos_contador = Usuario.objects.filter(pk=request.user.pk).annotate(num_grupos=Count('grupo'))[:1][0]

        return {'usuario_mensagem': usuario_mensagem, 'colaborador_contador': colaborador_contador,
                'contatos_contador': contatos_contador, 'grupos_contador': grupos_contador}
    return {}