/**
 * Created by Wouker on 20/01/14.
 */

$("[type=tel]").bind('input propertychange', function () {
    var texto = $(this).val();
    texto = texto.replace(/[^\d]/g, '');
    if (texto.length > 0) {
        texto = "(" + texto;
        if (texto.length > 3) {
            texto = [texto.slice(0, 3), ") ", texto.slice(3)].join('');
        }
        if (texto.length > 12) {
            if (!(texto.length > 13))
                texto = [texto.slice(0, 9), "-", texto.slice(9)].join('');
            else
                texto = [texto.slice(0, 10), "-", texto.slice(10)].join('');
        }
        if (texto.length == 15 && !(texto.substr(0, 6) > "(11) 5" && texto.substr(0, 3) <= "(11) 9"))
            texto = texto.substr(0, 14);
        var texto1 = texto.replace(/[^\d]/g, '');
        texto1 = ["(" + texto1.slice(0, 2), ") " + texto1.slice(2, 6), "-", texto1.slice(6)].join('');
        texto = texto1;
        if (texto.length > 15)
            texto = texto.substr(0, 15);
    }
    $(this).val(texto);
});