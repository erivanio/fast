function contarCaracteres(box,valor,campospan){
    var texto_validado = box.replace(/\n/g, "").replace(/\r/g, "");
	var conta = valor - texto_validado.length;
	document.getElementById(campospan).innerHTML = "Você ainda pode digitar " + conta + " caracteres";
	if(box.length >= valor){
		document.getElementById(campospan).innerHTML = "Opss.. você não pode mais digitar..";
		document.getElementById("campo").value = document.getElementById("campo").value.substr(0,valor);
	}
}
